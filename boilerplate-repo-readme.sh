#!/bin/bash

# Clear screen
clear

# Initialize variables
projectName=""
clientName=""
hostingPlatform=""

projectVariables=(projectName clientName hostingPlatform test)
missingProjectVariables=()

# Include colorLogger functions
source <(wget -qO- https://bitbucket.org/roundhouse-dev/rh-public-scripts/raw/master/libs/colorLogger.sh)

log -y "This script has a package dependency: dialog"

# Install Dependencies
if [ $(dpkg-query -W -f='${Status}' dialog 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
  read -p "Would you like to install it? [Y/n]" answer
  
  if [[ $answer == "" ]]; then
    log -g "Installing..."
    sudo apt-get install dialog;
  else
    echo "Exiting..."
    exit
  fi
else
  log -g "Dependency verified: dialog\n"
fi

# File name
project=PROJECT.md

# Check if it already exists
FILE=$project     
if [ -f $FILE ]; then
   echo "File $FILE already exists."
   #return
fi

# Project Name, REQUIRED, retry on no input
while [[ $projectName == '' ]] # While string is different or empty...
do
  read -p "Project name: " projectName
  if [[ $projectName == "" ]]; then
    log -r "You must enter a project name."
  fi
done 

# Client name with default
read -p "Client name: [Roundhouse] " clientName
if [[ $clientName == "" ]]; then
  clientName="Roundhouse"
fi

# Client name with default
read -p "Hosting Platform: [Heroku] " hostingPlatform
if [[ $hostingPlatform == "" ]]; then
  hostingPlatform="Heroku"
fi

# Test
read -p "Test: " test

# Check for empty variables
for i in "${projectVariables[@]}"
do
  if [[ ${!i} == "" ]]; then
    missingProjectVariables+=(${i})
  fi
done

log -r "\n⚠⇘"
log -r "Missing variables:"
echo "  ${missingProjectVariables[*]}"
log -r "⚠⇗\n"

# Save file
(
cat <<PROJECT
# $projectName

### Client

$clientName

---

### Hosting

Platform: $hostingPlatform
PROJECT
) > $project

# Read and display file for debugging
cat $project

# Remove saved file
#rm $project