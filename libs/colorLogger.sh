# Set up colorized log() function
# Example: log "This is some text"    # Log to console with default console color
# Example: log -r "This is some text" # Log text as red
log(){

  # Set up colors
  # Reset
  Color_Off='\033[0m'       # Text Reset

  # Regular Colors
  Red='\033[0;31m'          # log -r "This is some text"
  Green='\033[0;32m'        # log -g "This is some text"
  Yellow='\033[0;33m'       # log -y "This is some text"
  Blue='\033[0;34m'         # log -b "This is some text"
  Purple='\033[0;35m'       # log -p "This is some text"
  Cyan='\033[0;36m'         # log -c "This is some text"

  if [[ $2 == "" ]]; then
    color=White
    text=$1
  else
    OPTIND=1
    while getopts rgybpc: opt; do
      case $opt in
      r) color=Red
        text=$2
        ;;
      g) color=Green
        text=$2
        ;;
      y) color=Yellow
        text=$2
        ;;
      r) color=Red
        text=$2
        ;;
      g) color=Green
        text=$2
        ;;
      y) color=Yellow
        text=$2
        ;;
      *) ;;
      esac
    done
  fi
  
  echo -e "${!color}${text}${Color_Off}"
}